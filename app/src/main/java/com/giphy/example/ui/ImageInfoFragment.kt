package com.giphy.example.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.giphy.example.GiphyApp
import com.giphy.example.R
import com.giphy.example.db.entity.Image
import com.giphy.example.util.getSpannedFromString
import com.giphy.example.viewmodel.ImageInfoViewModel
import kotlinx.android.synthetic.main.fragment_image_info.*
import javax.inject.Inject

class ImageInfoFragment : Fragment() {

    companion object {
        val TAG = ImageInfoFragment::class.java.simpleName

        private const val IMAGE_ID_EXTRA = "image_id_extra"

        fun newInstance(imageId: String): ImageInfoFragment {
            val args = Bundle()
            args.putString(IMAGE_ID_EXTRA, imageId)

            val fragment = ImageInfoFragment()
            fragment.arguments = args

            return fragment
        }
    }

    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var imageInfoViewModel: ImageInfoViewModel

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        GiphyApp[context!!].component.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_image_info, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        imageInfoViewModel = ViewModelProviders.of(this, viewModelFactory).get(ImageInfoViewModel::class.java)

        val imageId = arguments?.getString(IMAGE_ID_EXTRA)

        setupViews()
        setupObservers(imageId!!)
    }

    override fun onSaveInstanceState(outState: Bundle) {
    }

    private fun setupViews() {
        setImageEnabled(false)

    }

    private fun setupObservers(imageId: String) {
        imageInfoViewModel.getImage(imageId).observe(this, Observer { handleImage(it) })
    }

    private fun handleImage(image: Image?) {

        if (image == null) {
            setImageEnabled(false)
            return
        }

        image_not_found.visibility = View.GONE

        Glide.with(this).load(image.pathOriginal).into(base_image)
        base_image.visibility = View.VISIBLE

        user_name.visibility = View.VISIBLE
        val user = image.user
        if (user == null) {
            user_name.text = getString(R.string.no_user)
            return
        }

        user_name.text = getString(R.string.username_plus_realname, user.displayName, user.username)

        user_profile.text = getSpannedFromString(getString(R.string.profile_link, user.profileUrl))
        user_profile.movementMethod = LinkMovementMethod.getInstance()
        user_profile.visibility = View.VISIBLE

        val twitter = user.twitter
        if (twitter != null) {
            user_twitter.text = getString(R.string.twitter_format, user.twitter)
            user_twitter.visibility = View.VISIBLE
        }
    }

    private fun setImageEnabled(flag: Boolean) {
        base_image.visibility = if (flag) View.VISIBLE else View.GONE
        user_name.visibility = if (flag) View.VISIBLE else View.GONE
        user_profile.visibility = if (flag) View.VISIBLE else View.GONE
        user_twitter.visibility = if (flag) View.VISIBLE else View.GONE
        image_not_found.visibility = if (flag) View.GONE else View.VISIBLE
    }
}