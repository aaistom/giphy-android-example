package com.giphy.example.ui

import android.arch.paging.PagedListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.giphy.example.R
import com.giphy.example.db.entity.Image
import kotlinx.android.synthetic.main.holder_image.view.*

class ImageListAdapter(private val clickListener: OnClickListener) :
        PagedListAdapter<Image, ImageListAdapter.ViewHolder>(DIFF_CALLBACK) {

    companion object {

        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Image>() {
            override fun areItemsTheSame(i1: Image, i2: Image): Boolean {
                return i1.id == i2.id
            }

            override fun areContentsTheSame(i1: Image, i2: Image): Boolean {
                return i1 == i2
            }
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindTo(image: Image, listener: OnClickListener) {

            Glide.with(itemView).load(image.pathPreview).into(itemView.image)

            itemView.setOnClickListener { _ -> listener.onClick(image) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.holder_image, parent, false)

        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val image = getItem(position)
        if (image != null) {
            holder.bindTo(image, clickListener)
        }
    }

    interface OnClickListener {
        fun onClick(image: Image)
    }
}