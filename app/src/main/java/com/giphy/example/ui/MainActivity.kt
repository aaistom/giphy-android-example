package com.giphy.example.ui

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import com.giphy.example.R
import com.giphy.example.db.entity.Image


class MainActivity : AppCompatActivity(), ImageListFragment.EventListener {

    private var deepLinkIntent: Intent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.addOnBackStackChangedListener(fragmentBackStackListener)
        deepLinkIntent = intent

        if (savedInstanceState == null) {
            showFirstFragment()
        }
    }

    override fun onNewIntent(intent: Intent?) {
        deepLinkIntent = intent
    }

    override fun onResume() {
        super.onResume()

        setupActionBar()
        deepLinkIntent?.let { testDeepLink(it) }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
    }

    override fun onImageClicked(image: Image) {
        showImageInfo(image.uuid)
    }

    private fun showActionBar(title: String, backArrow: Boolean) {
        val actionBar = supportActionBar ?: return

        actionBar.setDisplayHomeAsUpEnabled(backArrow)
        actionBar.setDisplayShowHomeEnabled(backArrow)
        actionBar.title = title
    }

    private fun showFirstFragment() {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment, ImageListFragment.newInstance(), ImageListFragment.TAG)
                .commit()
    }

    private fun testDeepLink(intent: Intent) {
        val action = intent.action
        if (Intent.ACTION_VIEW == action)
            intent.data?.let { showImageInfo(it.lastPathSegment) }

        deepLinkIntent = null
    }

    private fun showImageInfo(imageUuid: String) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment, ImageInfoFragment.newInstance(imageUuid), ImageInfoFragment.TAG)
                .addToBackStack(null)
                .commit()
    }

    private fun getCurrentFragment(): String {
        val current = supportFragmentManager.findFragmentById(R.id.fragment)

        when (current) {

            is ImageListFragment -> {
                return ImageListFragment.TAG
            }

            is ImageInfoFragment -> {
                return ImageInfoFragment.TAG
            }
        }

        return ""
    }

    private fun setupActionBar() {
        val tag = getCurrentFragment()
        if (tag == ImageListFragment.TAG)
            showActionBar(getString(R.string.app_name), false)
        else if (tag == ImageInfoFragment.TAG)
            showActionBar(getString(R.string.image_info), true)
    }

    private val fragmentBackStackListener = object : FragmentManager.OnBackStackChangedListener {
        override fun onBackStackChanged() {
            setupActionBar()
        }

    }
}
