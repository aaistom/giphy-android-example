package com.giphy.example.ui

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.arch.paging.PagedList
import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.giphy.example.GiphyApp
import com.giphy.example.R
import com.giphy.example.db.entity.Image
import com.giphy.example.util.Resource
import com.giphy.example.util.Status
import com.giphy.example.viewmodel.ImageListViewModel
import com.muddzdev.styleabletoastlibrary.StyleableToast
import kotlinx.android.synthetic.main.fragment_image_list.*
import javax.inject.Inject

class ImageListFragment : Fragment() {

    companion object {
        val TAG = ImageListFragment::class.java.simpleName

        const val LIST_STATE_EXTRA = "list_state_extra"

        fun newInstance(): ImageListFragment = ImageListFragment()
    }

    @Inject
    protected lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var imageListViewModel: ImageListViewModel

    private lateinit var eventListener: EventListener

    private var listState: Parcelable? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        GiphyApp[context!!].component.inject(this)

        try {
            eventListener = context as EventListener
        } catch (ex: ClassCastException) {
            throw ClassCastException(context.toString() + " must implement EventListener")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        listState = savedInstanceState?.getParcelable(LIST_STATE_EXTRA)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_image_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        imageListViewModel = ViewModelProviders.of(this, viewModelFactory).get(ImageListViewModel::class.java)

        setupViews()
        setupObservers()
    }

    override fun onPause() {
        saveListState(image_list.layoutManager)

        super.onPause()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putParcelable(LIST_STATE_EXTRA, listState)
    }

    private fun setupViews() {
        image_list.setHasFixedSize(true)
        image_list.layoutManager = GridLayoutManager(context, 2, LinearLayoutManager.VERTICAL, false)
        image_list.adapter = ImageListAdapter(imageClickListener)

        swipe_refresh_layout.setOnRefreshListener(swipeRefreshListener)

        load_more.visibility = View.GONE
    }

    private fun setupObservers() {
        imageListViewModel.imageList.observe(this, Observer { handleImageList(it!!) })
        imageListViewModel.getSwipeRefreshStatus().observe(this, Observer { handleSwipeRefreshStatus(it!!) })
        imageListViewModel.getLoadMoreStatus().observe(this, Observer { handleLoadMoreStatus(it!!) })
    }

    private fun saveListState(lm: RecyclerView.LayoutManager) {
        listState = lm.onSaveInstanceState()
    }

    private fun restoreListState(lm: RecyclerView.LayoutManager) {
        if (listState != null) {
            lm.onRestoreInstanceState(listState)
            listState = null
        }
    }

    private val imageClickListener = object : ImageListAdapter.OnClickListener {
        override fun onClick(image: Image) {

            eventListener.onImageClicked(image)
        }
    }

    private val swipeRefreshListener = object : SwipeRefreshLayout.OnRefreshListener {
        override fun onRefresh() {
            imageListViewModel.refreshImageList()
        }
    }

    private fun handleImageList(list: PagedList<Image>) {
        no_images_text.visibility = if (list.isEmpty()) View.VISIBLE else View.GONE

        val adapter = image_list.adapter as ImageListAdapter
        adapter.submitList(list)

        restoreListState(image_list.layoutManager)
    }

    private fun handleSwipeRefreshStatus(res: Resource<Any>) {

        when (res.status) {

            Status.LOADING -> {
                swipeRefreshEnabled(true)
            }

            Status.ERROR -> {
                swipeRefreshEnabled(false)
                res.message?.let {
                    StyleableToast.makeText(context!!, it, R.style.ToastError).show()
                }
            }

            Status.SUCCESS -> {
                swipeRefreshEnabled(false)
            }
        }
    }

    private fun handleLoadMoreStatus(res: Resource<Any>) {
        when (res.status) {

            Status.LOADING -> {
                load_more.visibility = View.VISIBLE
            }

            Status.ERROR -> {
                res.message?.let {
                    StyleableToast.makeText(context!!, it, R.style.ToastError).show()
                }

                load_more.visibility = View.GONE
            }

            Status.SUCCESS -> {
                load_more.visibility = View.GONE
            }
        }
    }

    private fun swipeRefreshEnabled(flag: Boolean) {
        swipe_refresh_layout.isRefreshing = flag
    }

    interface EventListener {
        fun onImageClicked(image: Image)
    }
}