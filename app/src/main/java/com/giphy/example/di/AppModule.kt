package com.giphy.example.di

import android.content.Context
import com.giphy.example.util.AppErrorHandler
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideContext(): Context = context

    @Provides
    @Singleton
    fun provideAppErrorHandler(context: Context, moshi: Moshi) = AppErrorHandler(context, moshi)

}