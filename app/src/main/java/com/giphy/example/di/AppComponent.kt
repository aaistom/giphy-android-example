package com.giphy.example.di

import com.giphy.example.ui.ImageInfoFragment
import com.giphy.example.ui.ImageListFragment
import com.giphy.example.ui.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, DatabaseModule::class, ViewModelModule::class, WebserviceModule::class])
interface AppComponent {

    fun inject(activity: MainActivity)

    fun inject(fragment: ImageInfoFragment)

    fun inject(fragment: ImageListFragment)
}