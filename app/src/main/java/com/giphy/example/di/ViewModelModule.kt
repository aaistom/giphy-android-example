package com.giphy.example.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.giphy.example.viewmodel.GiphyViewModelFactory
import com.giphy.example.viewmodel.ImageInfoViewModel
import com.giphy.example.viewmodel.ImageListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ImageListViewModel::class)
    abstract fun bindImageListViewModel(imageListViewModel: ImageListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ImageInfoViewModel::class)
    abstract fun bindImageInfoViewModel(imageInfoViewModel: ImageInfoViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: GiphyViewModelFactory): ViewModelProvider.Factory
}