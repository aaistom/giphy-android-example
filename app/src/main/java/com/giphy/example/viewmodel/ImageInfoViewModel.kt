package com.giphy.example.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.LiveDataReactiveStreams
import com.giphy.example.db.entity.Image
import com.giphy.example.repo.ImageRepository
import javax.inject.Inject

class ImageInfoViewModel @Inject
    constructor(private val repository: ImageRepository) : BaseViewModel() {

    fun getImage(uuid: String):LiveData<Image> {
        return LiveDataReactiveStreams.fromPublisher(repository.getImageByUuid(uuid))
    }
}