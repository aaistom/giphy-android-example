package com.giphy.example.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.LiveDataReactiveStreams
import android.arch.lifecycle.MutableLiveData
import android.arch.paging.PagedList
import android.util.Log
import com.giphy.example.db.entity.Image
import com.giphy.example.repo.ImageRepository
import com.giphy.example.util.AppErrorHandler
import com.giphy.example.util.Resource
import com.giphy.example.util.Status
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ImageListViewModel @Inject
constructor(private val repository: ImageRepository, private val errorHandler: AppErrorHandler) : BaseViewModel() {

    val imageList: LiveData<PagedList<Image>>
        get() = LiveDataReactiveStreams.fromPublisher(repository.getPagedImage(boundaryCallback))


    private val boundaryCallback = object : PagedList.BoundaryCallback<Image>() {
        override fun onZeroItemsLoaded() {
            refreshImageList()
        }

        override fun onItemAtEndLoaded(itemAtEnd: Image) {
            loadMoreImages()
        }

        override fun onItemAtFrontLoaded(itemAtFront: Image) {
            /* nothing */
        }
    }

    private val swipeRefreshStatus = MutableLiveData<Resource<Any>>()
    fun getSwipeRefreshStatus(): LiveData<Resource<Any>> {
        return swipeRefreshStatus
    }

    fun refreshImageList() {
        if (isLoading(swipeRefreshStatus.value)) {
            swipeRefreshStatus.value = Resource.loading(null)
            addDisposable(repository.loadFirstPage()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::handleSwipeRefreshSuccess, this::handleSwipeRefreshError))
        }
    }

    private fun handleSwipeRefreshSuccess() {
        swipeRefreshStatus.value = Resource.success(null)
    }

    private fun handleSwipeRefreshError(throwable: Throwable) {
        swipeRefreshStatus.value = Resource.error(errorHandler.recognize(throwable), null)
    }

    private val loadMoreStatus = MutableLiveData<Resource<Any>>()
    fun getLoadMoreStatus(): LiveData<Resource<Any>> {
        return loadMoreStatus
    }

    fun loadMoreImages() {
        if (isLoading(loadMoreStatus.value)) {
            loadMoreStatus.value = Resource.loading(null)
            addDisposable(repository.loadNextPage()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::handleLoadMoreSuccess, this::handleLoadMoreError))
        }
    }

    private fun handleLoadMoreSuccess() {
        loadMoreStatus.value = Resource.success(null)
    }

    private fun handleLoadMoreError(throwable: Throwable) {
        loadMoreStatus.value = Resource.error(errorHandler.recognize(throwable), null)
    }

    private fun isLoading(res: Resource<Any>?): Boolean {
        if (res == null)
            return true

        return res.status != Status.LOADING
    }

}