package com.giphy.example.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.giphy.example.db.entity.Image

@Database(entities = [Image::class], exportSchema = false, version = 1)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        const val DB_NAME = "giphy-db"

        fun build(context: Context): AppDatabase =
                Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME)
                        .fallbackToDestructiveMigration()
                        .build()
    }

    abstract fun imageDao(): ImageDao
}