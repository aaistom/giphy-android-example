package com.giphy.example.db.entity

import android.arch.persistence.room.ColumnInfo

data class User(
        val username: String,

        @ColumnInfo(name = "display_name")
        val displayName: String,

        @ColumnInfo(name = "profile_url")
        val profileUrl: String,

        val twitter: String?
)