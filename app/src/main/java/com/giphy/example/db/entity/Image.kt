package com.giphy.example.db.entity

import android.arch.persistence.room.*

@Entity(tableName = "image",
        indices = [Index(value = arrayOf("uuid"), unique = true)])
data class Image(
        @PrimaryKey(autoGenerate = true)
        val id: Int,
        val uuid: String,

        @ColumnInfo(name = "path_preview")
        val pathPreview: String,

        @ColumnInfo(name = "path_original")
        val pathOriginal: String,

        @Embedded(prefix = "user")
        val user: User?

)