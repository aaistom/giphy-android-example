package com.giphy.example.db

import android.arch.paging.DataSource
import android.arch.persistence.room.*
import com.giphy.example.db.entity.Image
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Single

@Dao
abstract class ImageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(images: List<Image>): List<Long>

    @Query("SELECT * FROM image WHERE uuid=:uuid")
    abstract fun loadImageByUuid(uuid: String): Flowable<Image>

    @Query("SELECT * FROM image ORDER BY id ASC")
    abstract fun loadPagedImages(): DataSource.Factory<Int, Image>

    @Query("DELETE FROM image")
    abstract fun removeAllImages()

    @Query("SELECT COUNT(*) FROM image")
    abstract fun getNumberOfImages(): Single<Int>
}