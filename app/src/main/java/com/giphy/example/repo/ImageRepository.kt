package com.giphy.example.repo

import android.arch.paging.PagedList
import android.arch.paging.RxPagedListBuilder
import com.giphy.example.api.WebService
import com.giphy.example.api.response.TrendingResponse
import com.giphy.example.api.toImage
import com.giphy.example.db.ImageDao
import com.giphy.example.db.entity.Image
import com.giphy.example.util.StatusCodeException
import com.giphy.example.util.getApiKey
import io.reactivex.BackpressureStrategy
import io.reactivex.Completable
import io.reactivex.Flowable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ImageRepository @Inject
constructor(private val service: WebService, private val cache: ImageDao) {

    companion object {
        private const val NETWORK_PAGE_SIZE = 25
        private const val LOCAL_PAGE_SIZE = 55

        private const val HTTP_SUCCESS_CODE = 200
    }

    fun loadFirstPage(): Completable {
        return service.getTrendingGifs(getApiKey(), 0, NETWORK_PAGE_SIZE)
                .doOnSuccess(this::testResponse)
                .map(this::transformData)
                .doOnSuccess({
                    cache.removeAllImages()
                    cache.insert(it)
                })
                .ignoreElement()
    }

    fun loadNextPage(): Completable {

        return cache.getNumberOfImages()
                .flatMap({ service.getTrendingGifs(getApiKey(), it, NETWORK_PAGE_SIZE) })
                .doOnSuccess(this::testResponse)
                .map(this::transformData)
                .doOnSuccess({ cache.insert(it) })
                .ignoreElement()

    }

    fun getImageByUuid(uuid: String): Flowable<Image> {
        return cache.loadImageByUuid(uuid)
    }

    fun getPagedImage(callback: PagedList.BoundaryCallback<Image>): Flowable<PagedList<Image>> {
        return RxPagedListBuilder(cache.loadPagedImages(), LOCAL_PAGE_SIZE)
                .setBoundaryCallback(callback)
                .buildFlowable(BackpressureStrategy.LATEST)
    }

    private fun testResponse(res: TrendingResponse) {
        val meta = res.meta
        if (meta.status != HTTP_SUCCESS_CODE) {
            throw StatusCodeException(meta.msg, meta.status)
        }
    }

    private fun transformData(res: TrendingResponse): List<Image> {
        val gifs = res.gifs

        val result = ArrayList<Image>(gifs.size)

        for (gif in gifs)
            result.add(gif.toImage())

        return result
    }
}