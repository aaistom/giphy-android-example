package com.giphy.example.api.response

import com.giphy.example.api.pojo.Gif
import com.giphy.example.api.pojo.Meta
import com.giphy.example.api.pojo.Pagination
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class TrendingResponse(

        @Json(name = "data")
        val gifs: List<Gif>,

        val meta: Meta,

        val pagination: Pagination
)