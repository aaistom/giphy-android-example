package com.giphy.example.api.pojo

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Image(
        @Json(name = "fixed_height")
        val fixedHeight: ImageData,

        @Json(name = "fixed_width")
        val fixedWidth: ImageData,

        @Json(name = "480w_still")
        val w480: ImageData

)