package com.giphy.example.api.pojo

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Gif(

        val id: String,

        @Json(name = "user")
        val user: RemoteUser?,

        @Json(name = "images")
        val image: Image
)