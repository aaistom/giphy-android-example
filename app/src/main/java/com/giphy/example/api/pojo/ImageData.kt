package com.giphy.example.api.pojo

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ImageData(
        val url: String,
        val width: Int,
        val height: Int
)