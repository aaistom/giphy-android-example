package com.giphy.example.api.pojo

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Meta(
        val status: Int,
        val msg: String
)