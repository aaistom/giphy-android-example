package com.giphy.example.api

import com.giphy.example.api.response.TrendingResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WebService {

    @GET("v1/gifs/trending")
    fun getTrendingGifs(@Query("api_key") token: String,
                        @Query("offset") offset: Int,
                        @Query("limit") limit: Int): Single<TrendingResponse>

}