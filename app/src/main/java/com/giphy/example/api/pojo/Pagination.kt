package com.giphy.example.api.pojo

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Pagination(
        val offset: Int,

        val count: Int,

        @Json(name = "total_count")
        val total: Int
)