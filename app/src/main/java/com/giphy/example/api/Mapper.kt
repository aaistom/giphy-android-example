package com.giphy.example.api

import com.giphy.example.api.pojo.Gif
import com.giphy.example.api.pojo.RemoteUser
import com.giphy.example.db.entity.Image
import com.giphy.example.db.entity.User

fun Gif.toImage(): Image {
    val previewUrl = image.w480.url
    val originalUrl = image.fixedWidth.url

    return Image(0, id, previewUrl, originalUrl, user.toUserOrNull())
}

private fun RemoteUser?.toUserOrNull(): User? {
    if (this == null)
        return null

    return User(username, displayName, profileUrl, twitter)
}