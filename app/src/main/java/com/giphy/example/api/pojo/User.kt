package com.giphy.example.api.pojo

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RemoteUser(
        val username: String,

        @Json(name = "display_name")
        val displayName: String,

        @Json(name = "profile_url")
        val profileUrl: String,

        val twitter: String?

)