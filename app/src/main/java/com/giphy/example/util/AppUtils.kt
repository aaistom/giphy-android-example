package com.giphy.example.util

import com.giphy.example.BuildConfig
import android.text.Html
import android.os.Build
import android.text.Spanned


fun getApiUrl(subpath: String): String {
    return BuildConfig.HOST + subpath
}

fun getApiKey(): String {
    return BuildConfig.KEY
}

fun getSpannedFromString(str: String): Spanned {

    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        Html.fromHtml(str, Html.FROM_HTML_MODE_LEGACY)
    } else {
        Html.fromHtml(str)
    }
}