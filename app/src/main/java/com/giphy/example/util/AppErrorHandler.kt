package com.giphy.example.util

import android.content.Context
import com.giphy.example.R
import com.squareup.moshi.Moshi
import okhttp3.ResponseBody
import retrofit2.HttpException

class AppErrorHandler(private val context: Context, private val moshi: Moshi) {

    fun recognize(throwable: Throwable): String {

        throwable.printStackTrace()

        return when (throwable) {

            is HttpException -> {
                val message = tryErrorBody(throwable.response().errorBody())
                message ?: context.getString(R.string.server_bad_response)
            }

            is StatusCodeException -> {
                context.getString(R.string.bad_status_code)
            }

            else -> {
                context.getString(R.string.unknown_error)
            }
        }


    }

    private fun tryErrorBody(response: ResponseBody?): String? {
        if (response == null)
            return null

        return try {
            val body = moshi.adapter(ErrorBody::class.java).fromJson(response.string())
            body?.message
        } catch (ex: Exception) {
            null
        }
    }
}