package com.giphy.example.util

class StatusCodeException(message: String, val statusCode: Int) : RuntimeException(message)