package com.giphy.example.util

enum class Status { LOADING, ERROR, SUCCESS }