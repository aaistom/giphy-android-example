package com.giphy.example.util

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ErrorBody(val message: String)