package com.giphy.example

import android.app.Application
import android.content.Context
import com.giphy.example.di.*

class GiphyApp : Application() {

    companion object {
        operator fun get(context: Context): GiphyApp {
            return context.applicationContext as GiphyApp
        }
    }

    private lateinit var appComponent: AppComponent

    val component: AppComponent
        get() {
            return appComponent
        }

    override fun onCreate() {
        super.onCreate()

        initDagger()
    }

    private fun initDagger() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .databaseModule(DatabaseModule())
                .webserviceModule(WebserviceModule())
                .build()
    }
}